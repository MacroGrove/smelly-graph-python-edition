from turtle import *

def plot(expr):
    max = find_max(expr)
    if max == 1:
        max = 1
    zoom = 300 / max

    up()
    setpos(-300, expr.eval(-10)*zoom)
    down()
    
    speed(0)
    for x in range(-300, 300, 1):
        setpos(x, expr.eval(x / 30)*zoom)

    done()

def find_max(expr, x1 = -10, x2 = 10, step = 0.01):
    max = abs(expr.eval(x1))
    for x in range(int(x1 / step), int(x2 / step)):
        if abs(expr.eval(x * step)) > max:
            max = abs(expr.eval(x * step))
    return max




