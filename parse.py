import math

class Eval:
    def __init__(self, fn):
        self.fn = fn

    def eval(self, x):
        return self.fn(x)


class Multiply(Eval):
    def __init__(self, expr1, expr2):
        def fn(x):
            return expr1.eval(x) * expr2.eval(x)
        super().__init__(fn)

class Divide(Eval):
    def __init__(self, expr1, expr2):
        def fn(x):
            e2 = expr2.eval(x)
            if e2 == 0:
                e2 = 1
            return expr1.eval(x) / e2
        super().__init__(fn)

class Add(Eval):
    def __init__(self, expr1, expr2):
        def fn(x):
            return expr1.eval(x) + expr2.eval(x)
        super().__init__(fn)

class Subtract(Eval):
    def __init__(self, expr1, expr2):
        def fn(x):
            return expr1.eval(x) - expr2.eval(x)
        super().__init__(fn)

class Pow(Eval):
    def __init__(self, expr1, expr2):
        def fn(x):
            return expr1.eval(x) ** expr2.eval(x)
        super().__init__(fn)

class Sin(Eval):
    def __init__(self, expr1):
        def fn(x):
            return math.sin(expr1.eval(x))
        super().__init__(fn)

class Cos(Eval):
    def __init__(self, expr1):
        def fn(x):
            return math.cos(expr1.eval(x))
        super().__init__(fn)

class Tan(Eval):
    def __init__(self, expr1):
        def fn(x):
            return math.tan(expr1.eval(x))
        super().__init__(fn)




class Variable(Eval):
        def __init__(self):
            def fn(x):
                return x
            super().__init__(fn)

class Value(Eval):
        def __init__(self, val):
            def fn(x):
                return val
            super().__init__(fn)

def parse_str(str):
    return parse(str.split())[0]

def parse(tokens):

    lhs = tokens[0]
    lhs_eval = None
    if(lhs == "x"):
        lhs_eval = Variable()
    elif(lhs in ["cos(", "sin(", "tan("]):
        inside_trig = parse(tokens[1:])

        if lhs == "cos(":
            lhs_eval = Cos(inside_trig[0])
        if lhs == "sin(":
            lhs_eval = Sin(inside_trig[0])
        if lhs == "tan(":
            lhs_eval = Tan(inside_trig[0])

        #whatever is left
        tokens = inside_trig[1]
    else:
        lhs_eval = Value(float(lhs))
        
    if(len(tokens) == 1):
        return lhs_eval, []
    if(tokens[1] == ")"):
        return lhs_eval, tokens[1:]

    expr = tokens[1]
    eval = None

    if(expr == "+"):
        eval = Add(lhs_eval, parse(tokens[2:])[0])
    elif(expr == "-"):
        eval = Subtract(lhs_eval, parse(tokens[2:])[0])
    elif(expr == "*"):
        eval = Multiply(lhs_eval, parse(tokens[2:])[0])
    elif(expr == "/"):
        eval = Divide(lhs_eval, parse(tokens[2:])[0])
    elif(expr == "^"):
        eval = Pow(lhs_eval, parse(tokens[2:])[0])

    return eval, tokens[2:]